# Owls Pricer Ext

Automatically label NC items with owls and valisar value. Visit ~/owls to refresh values, visit ~/valisar for cap values, also highlight wishlist items from JellyNeo wishlist and high value items (>10). 

Values items at:
- Neopets: classic inventory, gallery, closet, SDB
- JellyNeo: Item Database, Articles
- Classic DTI
- DTI 2020

## How to add JellyNeo wish list

At line38: Edit `wishListLink = ''` to `wishListLink = 'Your JN wish list page`.
```
    let wishListLink = ''; // <- Here
    // Example:
    // let wishListLink = 'https://items.jellyneo.net/mywishes/yuxian/278001/'
```

## Example

Example: God of Meepits' gallery.

![Example: God of Meepits' gallery](demo.png)



- Value in light blue: Unknown value items (`?` or `00 - 00`)
- Value in Meepit pink: High value item (value > 10)
- Value in orange: Wish list item. 
