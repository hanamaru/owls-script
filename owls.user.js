// ==UserScript==
// @name         Owls Pricer Ext
// @version      1.04
// @description  Automatically label NC items with waka value. Visit ~/owls to refresh values, visit https://www.neopets.com/~valisar for cap values.
// @author       Meep+
// @match        http*://www.neopets.com/~owls
// @match        http*://www.neopets.com/~Owls
// @match        http*://www.neopets.com/~OWLS
// @match        http*://www.neopets.com/~valisar
// @match        http*://www.neopets.com/~Valisar
// @match        http*://www.neopets.com/inventory.phtml*
// @match        http*://www.neopets.com/closet.phtml*
// @match        http*://www.neopets.com/safetydeposit.phtml*
// @match        http*://www.neopets.com/gallery/*
// @match        http*://items.jellyneo.net/*
// @match        http*://www.jellyneo.net/?go=*
// @match        http*://impress.openneo.net/*
// @match        https://impress-2020.openneo.net/*
// @require      http://code.jquery.com/jquery-latest.js
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

/**
 * Original script by @friendly-trenchcoat
 * Waka script:
 * https://github.com/friendly-trenchcoat/Waka-Values/blob/main/wakaValues.user.js
 *
 * Owls script:
 * https://pastebin.com/mmPtS3Q3
 *
 * Add Wish list/cap value to owls script.
 */

 (function () {
    'use strict';
    console.log('Owls script loaded.');

    // Set wish list here, only works for JN item wish list.
    // let wishListLink = '';
    // Example:
    let wishListLink = 'https://items.jellyneo.net/mywishes/yuxian/278001/'
    let OWLS, OWLS_EXT, OWLS_NOTE, wishList;
    OWLS_EXT = {};
    OWLS_NOTE = {};
    let WISH_LIST = {};

    // Init
    if (document.URL.toLowerCase().includes("~owls")) {
        getOwlsValue();
    } else if (document.URL.toLowerCase().includes("~valisar")){
        getCapValue();
    } else if (document.URL === wishListLink) {
        getWishList();
    } else {
        init();
    }

    function getOwlsValue() {
        /* Get cash item value from owls page. */
        let items_arr = document.getElementsByClassName("content-box")[0].innerText.split('\n');
        let items = items_arr.reduce((o, item) => {
            let split = item.split(' ~');
            if (split.length === 2) o[split[0]] = split[1];
            return o;
        }, {});
        GM_setValue("NEOPETS_OWLS", JSON.stringify(items));
        console.log('OWLS values updated.');
    }

    function getCapValue() {
        let cap_value = {}
        let cap_notes = {}
        let items_arr = document.getElementsByTagName("TR"); // Get items ;w;
        for (let i = 0; i < items_arr.length; i++) {
            let cap_items = items_arr[i].getElementsByTagName("td");
            if ((cap_items.length !== 4) || (cap_items[0].innerHTML === "RELEASE")){
                // Not an item
                continue;
            }
            else {
                cap_value[cap_items[1].innerHTML] = cap_items[2].innerHTML;
                cap_notes[cap_items[1].innerHTML] = cap_items[3].innerHTML;
            }
        }
        GM_setValue("NEOPETS_OWLS_EXT", JSON.stringify(cap_value));
        GM_setValue("NEOPETS_VALISAR_NOTE", JSON.stringify(cap_notes));
        console.log("Cap values updated.");
    }

    function getWishList() {
        let wishList = [];
        $('img.item-result-image.nc').each((i, el) => {
            const name = $(el).attr('title').split(' - r')[0];
            wishList.push(name);
        })
        GM_setValue("NEOPETS_OWLS_WISH", JSON.stringify(wishList));
        console.log("Wish list updated. ");
    }

    function getLocalOWLSData() {
        /* In case something goes wrong in Heroku */
        try {
            const OWLS = JSON.parse(GM_getValue("NEOPETS_OWLS"));
        } catch (e) {
            console.log('Visit OWLS to populate data: https://www.neopets.com/~OWLS');
        }
        return OWLS;
    }

    function init() {
        $.get("https://neo-owls.herokuapp.com/itemdata/owls_script/", function(data) {
            if (data) {
                console.log("Get OWLS data from Heroku");
                const OWLSstring = JSON.stringify(data);
                OWLS = JSON.parse(OWLSstring);
            } else {
                console.log("Get OWLS data from local");
                OWLS = getLocalOWLSData();
            }
            try {
                OWLS_EXT = JSON.parse(GM_getValue("NEOPETS_OWLS_EXT"));
                OWLS_NOTE = JSON.parse(GM_getValue("NEOPETS_VALISAR_NOTE"));
            } catch (err) {
                console.log("Visit https://www.neopets.com/~valisar for cap value.");
            } finally {
                OWLS = Object.assign(OWLS, OWLS_EXT);
            }
            try {
                WISH_LIST = JSON.parse(GM_getValue("NEOPETS_OWLS_WISH"));
            } catch(err) {
                ;
            } finally {
                if (OWLS) {
                    createCSS();
                    drawValues(OWLS);
                }
            }
        })

    }

    /* Add div with item value to items */

    function addClass(value, note='', isWish=false) {
        /* Mark item value div with difference color, and try to add hover for some trading data ;w;
         * CSS hover: https://sebhastian.com/html-hover-text/
         * @param {string} value: Cap value;
         * @param {string} note: Cap note.
         * @param {boolean} isWish: If an item is in wish list.
         * @return {object} HTML div object (probably) */

        let lowValue = value.match(/[-0-9]+/); // Match the low value
        const div = document.createElement('div');
        div.classList.add("OWLS");
        if (isWish) {
            div.classList.add("wishlist-item");
        } else if (value === '00 - 00' || value === '?') {
            div.classList.add("unknown-value");
        } else if (parseInt(lowValue) >= 10) {
            div.classList.add("high-value");
        }
        if (note.length > 0) {
            div.classList.add("hovertext");
            div.setAttribute("data-hover", note);
        }
        return div;
    }

    function findItem(itemName) {
        /* Find item in GM value with item name.
         * @param {string} itemName: item name.
         * @return {object} HTML div object (probably x 2) */
        let value = OWLS[itemName] || '?';
        let note = OWLS_NOTE[itemName] || '';
        let inWishList = false;
        if (Object.keys(WISH_LIST).length !== 0) {
            inWishList = WISH_LIST.includes(itemName)
        }
        let outerDiv = addClass(value, note, inWishList);
        // Create div like this: <div class="waka"><div>${value}</div></div>
        let innerDiv = document.createElement('div');
        innerDiv.innerHTML = value;
        outerDiv.appendChild(innerDiv);
        return outerDiv;
    }

    function isCashItem(itemName) {
        /* Check if an item is in OWLS/valisar database. Mainly for checking items in inventory/SDB.
         * Reference: https://stackoverflow.com/questions/8511281/check-if-a-value-is-an-object-in-javascript
         * @param {string} itemName: Item name.
         * @return {boolean} true if in NC trade DB.
        */
        return (typeof(OWLS[itemName]) != 'undefined' && OWLS[itemName] !== null )
    }

    /* Draw values */

    function drawValues(OWLS) {
        // stealin this
        jQuery.fn.justtext = function () {
            return $(this).clone().children().remove().end().text();
        };

        if (document.URL.includes("neopets.com/inventory")) {
            if ($('#navnewsdropdown__2020').length) {
                // Beta Inventory
                $(document).ajaxSuccess(function () {
                    $('.item-subname:contains("wearable"):contains("Neocash"):not(:contains("no trade"))').each(function (i, el) {
                        let $parent = $(el).parent();
                        if (!$parent.find('.OWLS').length) {
                            const name = $parent.find('.item-name').text();
                            $parent.children().eq(0).after(findItem(name));
                        }
                    });
                });
            } else {
                // Classic Inventory
                $('td.wearable:contains(Neocash)').each(function (i, el) {
                    const name = $(el).justtext();
                    $(el).append(findItem(name));
                });
            }
        }

        // Closet
        else if (document.URL.includes("neopets.com/closet")) {
            $('td>b:contains("Artifact - 500")').each(function (i, el) {
                const name = $(el).justtext();
                $(el).parent().prev().append(findItem(name));
            });
        }

        // SDB
        else if (document.URL.includes("neopets.com/safetydeposit")) {
            // 'tr[bgcolor="#DFEAF7"]:contains(Neocash)'
            $('tr:contains(Neocash)').each(function (i, el) {
                const name = $(el).find('b').first().justtext();
                let isWearable = $(el).css("background-color") == "#DFEAF7";
                if ((isWearable) || isCashItem(name)) {
                    $(el).children().eq(0).append(findItem(name))
                }
            });
        }

        // Gallery
        else if (document.URL.includes("neopets.com/gallery")) {
            $('td>b.textcolor').each(function (i, el) {
                const name = $(el).text();
                const value = OWLS[name];
                if (typeof(value) == "undefined") {
                    ;
                }
                else {
                    $(el).before(findItem(name));
                }
            });
        }

        // JNIDB
        else if (document.URL.includes("items.jellyneo.net")) {
            $('img.item-result-image.nc').each((i, el) => {
                const name = $(el).attr('title').split(' - r')[0];
                let $parent = $(el).parent();
                let $next = $parent.next();
                if ($next.is('br')) $next.remove();
                $parent.after(findItem(name));
            });
        }

        // JN Article
        else if (document.URL.includes("www.jellyneo.net")) {
            $('img[src*="/items/"]').each((i, el) => {
                const name = $(el).attr('title') || $(el).attr('alt');
                const value = OWLS[name];
                if (value) {
                    let $parent = $(el).parent();
                    let $next = $parent.next();
                    if ($next.is('br')) $next.remove();
                    $parent.after(findItem(name));
                }
            });
        }

        // Classic DTI Customize
        else if (document.URL.includes("impress.openneo.net/wardrobe")) {
            $(document).ajaxSuccess(function (event, xhr, options) {
                if (options.url.includes('/items')) {
                    $('img.nc-icon').each((i, el) => {
                        let $parent = $(el).parent();
                        if (!$parent.find('.OWLS').length) {
                            const name = $parent.text().match(/ (\S.*)  i /)[1];
                            $parent.children().eq(0).after(findItem(name));
                        }
                    });
                }
            });
        }
        // Classic DTI User Profile
        else if (document.URL.includes("impress.openneo.net/user/")) {
            $('img.nc-icon').each((i, el) => {
                let $parent = $(el).parent();
                if (!$parent.find('.OWLS').length) {
                    const name = $parent.find('span.name').text();
                    $parent.children().eq(0).after(findItem(name));
                }
            });
        }
        // Classic DTI Item
        else if (document.URL.includes("impress.openneo.net/items")) {
            if ($('img.nc-icon').length) {
                const name = $("#item-name").text();
                $("#item-name").after(findItem(name));
            }
            //$('header#item-header>div').append($(`<a href="https://impress-2020.openneo.net/items/search/${encodeURIComponent(name)}" target="_blank">DTI 2020</a>`));
            $('header#item-header>div').append($(`<a href="https://impress-2020.openneo.net/items/${$('#item-preview-header > a').attr('href').split('=').pop()}" target="_blank">DTI 2020</a>`));
        }

        else if (document.URL.includes("impress-2020.openneo.net/")) {
            // DTI 2020 & Thank you @Juvian
            const targetNode = document.getElementById('__next');
            const config = { attributes: true, childList: true, subtree: true };
            const callback = function(mutationsList, observer) {
                // Use traditional 'for loops' for IE 11
                for(let mutation of mutationsList) {
                    for (const added of mutation.addedNodes) {
                        if (!added.querySelectorAll) continue;

                        for (const node of [added.querySelectorAll('div[role="group"]')]) {
                            // For user WL/TL/Search result page,
                            // Works on those divs with "group" role
                            if (node.length === 0) continue;
                            for (const item of node) {
                                let [itemType, name] = item.innerText.split("\n");
                                if (itemType === "NC" && item.classList.contains("OWLS") == false) {
                                    item.querySelectorAll("div")[5].after(findItem(name));
                                }
                            }
                        }

                        for (const node of [added.getElementsByClassName('chakra-heading')]) {
                            // For item page.
                            // Those with class tag "chakra-heading"/'chakra-skeleton'
                            if (node.length === 0) continue;
                            for (const item of node) {
                                // Now item is the node ;w;
                                const name = item.innerText;
                                console.log(name);
                                if (name.length === 0) continue;
                                if (!isCashItem(name)) continue;
                                // In case some divs with header "friend's wish" ;w;
                                if (item.classList.contains("OWLS") == false) {
                                    // I know it's 🚮 but it works
                                    item.parentNode.parentNode.parentNode.firstChild.firstChild.after(findItem(name));
                                }
                            }

                        }
                    }
                }
            };
            const observer = new MutationObserver(callback);
            observer.observe(targetNode, config);
        }
    }

    function createCSS() {
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = `
        .OWLS {
            display: flex;
        }
        .OWLS>div {
            font-family: "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
            font-size: 12px;
            font-weight: bold;
            line-height: normal;
            text-align: center;
            color: #fff;
            background: #7199C5;
            border-radius: 10px;
            padding: 0.05em 0.5em;
            margin: 3px auto;
        }
        .unknown-value>div {
            background: #96bde1;
        }
        .high-value>div{
            background: #f69dcb;
        }

        .wishlist-item>div  {
            background: #F39801;
        }

        .hovertext {
          position: relative;
          border-bottom: 1px dotted black;
        }

        .hovertext:before {
          content: attr(data-hover);
          visibility: hidden;
          opacity: 0;
          background-color: #D4FFEA;
          width: 140px;
          color: #18452A;
          text-align: center;
          border-radius: 5px;
          padding: 5px 0;

          position: absolute;
          z-index: 1;
          left: 0;
          top: 110%;
        }

        .hovertext:hover:before {
          opacity: 1;
          visibility: visible;
        }
        `;
        document.body.appendChild(css);
    }
})();